<?php
/**
 * Created by PhpStorm.
 * User: aleksei
 * Date: 25.05.17
 * Time: 14:53
 */

namespace Happeak\Endpoint;

class Promotions extends AbstractEndpoint
{

    protected $endpoint = '/promotion/list';

    /**
     * Active promotions list
     *
     * @param int $offset
     *
     * @return \Psr\Http\Message\StreamInterface
     */
    public function getActive(int $offset = 0)
    {
        return $this->client->get($this->endpoint, [
            'only_active' => 1,
            'last_id'     => $offset,
        ]);
    }

    /**
     * Products list by promotion
     *
     * @param int $promotionId
     *
     * @return \Psr\Http\Message\StreamInterface
     */
    public function getProductsByPromotionId(int $promotionId)
    {
        return $this->client->get('/promotion/products', [
            'id' => $promotionId,
        ]);
    }
}