<?php
/**
 * Created by PhpStorm.
 * User: aleksei
 * Date: 16.08.17
 * Time: 17:47
 */

namespace Happeak\Endpoint;

class Delivery extends AbstractEndpoint
{

    protected $endpoint = '/delivery/points';

    /**
     * Get list of delivery points
     *
     * @param int $cityId
     *
     * @return \Psr\Http\Message\StreamInterface
     */
    public function getDeliveryPoints(int $cityId)
    {
        return $this->client->get($this->endpoint, [
            'city_id' => $cityId,
        ]);
    }
}