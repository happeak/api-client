<?php
/**
 * Created by PhpStorm.
 * User: aleksei
 * Date: 31.05.2018
 * Time: 16:47
 */

namespace Happeak\Endpoint;

class Faq extends AbstractEndpoint
{

    protected $endpoint = '/faq_group';

    /**
     * Получить данные по группе вопросов + вопросы по ней
     *
     * @param int $faqGroupId
     *
     * @return mixed
     */
    public function getOne(int $faqGroupId)
    {
        return $this->client->get($this->endpoint . '/' . $faqGroupId);
    }

    /**
     * Список вопросов по группе
     *
     * @param int $faqGroupId
     *
     * @return mixed
     */
    public function getQuestions(int $faqGroupId)
    {
        return $this->client->get($this->endpoint . '/' . $faqGroupId . '/questions');
    }
}