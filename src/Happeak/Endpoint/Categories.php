<?php
/**
 * Created by PhpStorm.
 * User: aleksei
 * Date: 12.01.17
 * Time: 14:22
 */

namespace Happeak\Endpoint;

class Categories extends AbstractEndpoint
{

    protected $endpoint = '/category/list';
}