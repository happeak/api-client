<?php
/**
 * Created by PhpStorm.
 * User: aleksei
 * Date: 12.01.17
 * Time: 13:42
 */

namespace Happeak\Endpoint;

class Products extends AbstractEndpoint
{

    protected $endpoint = '/product/list';

    /**
     * Products list filtered by the specified ids
     *
     * @param array $ids
     * @param int   $offset
     *
     * @return mixed
     */
    public function filterByIds(array $ids = [], int $offset = 0)
    {
        return $this->client->get(
            $this->endpoint,
            [
                'ids'     => $ids,
                'last_id' => $offset,
            ]
        );
    }

    /**
     * Products list filtered by the specified brand
     *
     * @param int $brand
     * @param int $offset
     *
     * @return mixed
     */
    public function filterByBrandId(int $brand, int $offset = 0)
    {
        return $this->client->get(
            $this->endpoint,
            [
                'brand'   => $brand,
                'last_id' => $offset,
            ]
        );
    }

    /**
     * Products list filtered by the specified category
     *
     * @param int $category
     * @param int $offset
     *
     * @return mixed
     */
    public function filterByCategoryId(int $category, int $offset = 0)
    {
        return $this->client->get(
            $this->endpoint,
            [
                'category' => $category,
                'last_id'  => $offset,
            ]
        );
    }

    /**
     * Products list filtered by the specified model
     *
     * @param int $model
     * @param int $offset
     *
     * @return mixed
     */
    public function filterByModelId(int $model, int $offset = 0)
    {
        return $this->client->get(
            $this->endpoint,
            [
                'model'   => $model,
                'last_id' => $offset,
            ]
        );
    }
}