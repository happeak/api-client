<?php
/**
 * Created by PhpStorm.
 * User: aleksei
 * Date: 12.01.17
 * Time: 14:40
 */

namespace Happeak\Endpoint;

class Order extends AbstractEndpoint
{

    protected $endpoint = '/order';

    /**
     * Создать заказ
     *
     * @param array $order
     * @param array $products
     *
     * @return mixed
     */
    public function create(array $order = [], array $products = [])
    {
        $orderDetails = [
            'products' => $products,
            'order'    => $order,
        ];

        return $this->client->post($this->endpoint . '/create', [], $orderDetails);
    }

    public function all(int $offset = 0)
    {
        return null;
    }
}