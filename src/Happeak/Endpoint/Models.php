<?php
/**
 * Created by PhpStorm.
 * User: aleksei
 * Date: 18.05.17
 * Time: 17:05
 */

namespace Happeak\Endpoint;

class Models extends AbstractEndpoint
{

    protected $endpoint = '/model/list';

    /**
     * Models list filtered by the specified brand
     *
     * @param int $brand
     * @param int $offset
     *
     * @return \Psr\Http\Message\StreamInterface
     */
    public function filterByBrandId(int $brand, int $offset = 0)
    {
        return $this->client->get(
            $this->endpoint,
            [
                'brand'   => $brand,
                'last_id' => $offset,
            ]
        );
    }

    /**
     * Models list filtered by the specified ids
     *
     * @param array $ids
     * @param int   $offset
     *
     * @return \Psr\Http\Message\StreamInterface
     */
    public function filterByIds(array $ids = [], int $offset = 0)
    {
        return $this->client->get(
            $this->endpoint,
            [
                'ids'     => $ids,
                'last_id' => $offset,
            ]
        );
    }
}