<?php

namespace Happeak\Endpoint;

class Blocks extends AbstractEndpoint
{

    protected $endpoint = '/content';

    protected $queryParameters = [
        'type' => 'block',
    ];
}