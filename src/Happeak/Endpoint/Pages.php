<?php

namespace Happeak\Endpoint;

class Pages extends AbstractEndpoint
{

    protected $endpoint = '/content';

    protected $queryParameters = [
        'type' => 'page',
    ];
}