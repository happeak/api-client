<?php

namespace Happeak\Endpoint;

class Subscribe extends AbstractEndpoint
{

    protected $endpoint = '/subscribe';

    /**
     * Subscribe to newsletter
     *
     * @param string $email
     * @param int    $listId
     * @param array  $params
     *
     * @return mixed
     */
    public function subscribe(string $email, int $listId = 2, array $params = [])
    {
        $params = array_merge([
            'email'   => $email,
            'name'    => $email,
            'list_id' => $listId,
        ], $params);

        return $this->client->post($this->endpoint . '/create', ['subscriber' => $params]);
    }
}