<?php

namespace Happeak\Endpoint;

class Menus extends AbstractEndpoint
{

    protected $endpoint = '/content';

    protected $queryParameters = [
        'type' => 'menu',
    ];
}