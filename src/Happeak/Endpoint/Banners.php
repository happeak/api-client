<?php

namespace Happeak\Endpoint;

class Banners extends AbstractEndpoint
{

    protected $endpoint = '/content';

    protected $queryParameters = [
        'type' => 'banner',
    ];
}