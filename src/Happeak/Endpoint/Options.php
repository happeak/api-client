<?php
/**
 * Created by PhpStorm.
 * User: aleksei
 * Date: 07.02.18
 * Time: 13:38
 */

namespace Happeak\Endpoint;

class Options extends AbstractEndpoint
{

    protected $endpoint = '/options';

    /**
     * @param int $offset
     *
     * @return mixed
     */
    public function all(int $offset = 0)
    {
        return $this->client->get($this->endpoint . '/list');
    }

    /**
     * @param array $ids
     *
     * @return mixed
     */
    public function filterByModelIds(array $ids = [])
    {
        return $this->client->get($this->endpoint . '/models', [
            'ids' => $ids,
        ]);
    }

    /**
     * @param array $ids
     *
     * @return mixed
     */
    public function filterByProductIds(array $ids = [])
    {
        return $this->client->get($this->endpoint . '/products', [
            'ids' => $ids,
        ]);
    }
}