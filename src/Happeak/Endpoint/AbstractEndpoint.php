<?php

namespace Happeak\Endpoint;

use Happeak\ApiClient;

abstract class AbstractEndpoint
{

    /**
     * @var ApiClient
     */
    protected $client;

    /**
     * @var string $endpoint
     */
    protected $endpoint;

    /**
     * @var array
     */
    protected $queryParameters = [];

    public function __construct(ApiClient $client)
    {
        $this->client = $client;
    }

    /**
     * @return array
     */
    public function getQueryParameters()
    {
        return $this->queryParameters;
    }

    /**
     * @param int $offset
     *
     * @return mixed
     */
    public function all(int $offset = 0)
    {
        return $this->client->get($this->endpoint, array_merge($this->getQueryParameters(), [
            'last_id' => $offset,
        ]));
    }

    /**
     * Фильтрация по критерию
     *
     * @param array $criteria
     *
     * @return mixed
     */
    public function filterByCriteria(array $criteria = [])
    {
        return $this->client->get($this->endpoint, $criteria);
    }
}