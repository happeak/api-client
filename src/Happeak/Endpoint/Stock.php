<?php

namespace Happeak\Endpoint;

class Stock extends AbstractEndpoint
{

    protected $endpoint = '/warehouse/stock';

    /**
     * @param int $id
     *
     * @return \Psr\Http\Message\StreamInterface
     */
    public function filterByWarehouse(int $id = 3)
    {
        return $this->client->get($this->endpoint, ['id' => $id]);
    }
}