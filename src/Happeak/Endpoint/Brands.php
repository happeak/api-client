<?php
/**
 * Created by PhpStorm.
 * User: aleksei
 * Date: 12.01.17
 * Time: 14:17
 */

namespace Happeak\Endpoint;

class Brands extends AbstractEndpoint
{

    protected $endpoint = '/brand/list';
}