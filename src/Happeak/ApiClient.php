<?php

namespace Happeak;

use GuzzleHttp\Client;
use Happeak\Endpoint\{AbstractEndpoint,
    Banners,
    Blocks,
    Brands,
    Categories,
    Delivery,
    Faq,
    Menus,
    Models,
    Options,
    Order,
    Pages,
    Products,
    Promotions,
    Stock,
    Subscribe};

/**
 * Class ApiClient
 * @package Happeak
 *
 * @property Brands $brands
 * @property Categories $categories
 * @property Delivery $delivery
 * @property Faq $faqs
 * @property Models $models
 * @property Order $order
 * @property Options $options
 * @property Products $products
 * @property Promotions $promotions
 * @property Stock $stock
 * @property Subscribe $subscribe
 * @property Pages $pages
 * @property Blocks $blocks
 * @property Banners $banners
 * @property Menus $menus
 */
class ApiClient
{

    /**
     * ID магазина
     * @var mixed
     */
    public $shopId, $shopKey, $url;

    /**
     * GuzzleHTTP Client instance
     * @var Client
     */
    public $httpClient;

    /**
     * ApiClient constructor.
     *
     * @param int $shopId
     * @param string $shopKey
     * @param string $url
     */
    public function __construct(int $shopId, string $shopKey, string $url = 'https://api.happeak.ru')
    {
        $this->shopId = $shopId;
        $this->shopKey = $shopKey;
        $this->url = $url;
    }

    /**
     * @param $property
     *
     * @return AbstractEndpoint
     */
    public function __get($property)
    {
        if (property_exists($this, $property)) {
            return $this->$property;
        }

        $namespace = '\\Happeak\\Endpoint\\' . ucfirst($property);

        return new $namespace($this);
    }

    /**
     * @return Client
     */
    public function getClient()
    {
        if (!$this->httpClient) {
            $this->httpClient = new Client([
                'base_uri' => $this->url,
            ]);
        }

        return $this->httpClient;
    }

    /**
     * GET http request
     *
     * @param       $url
     * @param array $query
     *
     * @return mixed
     */
    public function get(string $url, array $query = [])
    {
        $httpClient = $this->getClient();

        $query = array_merge($query, [
            'shop' => $this->shopId,
            'key'  => $this->shopKey,
        ]);

        $response = $httpClient->get($url, ['query' => $query]);
        $response = json_decode($response->getBody(), true);

        return $response;
    }

    /**
     * POST Http request
     *
     * @param       $url
     * @param array $query
     * @param array $params
     *
     * @return mixed
     */
    public function post(string $url, array $query = [], array $params = [])
    {
        $httpClient = $this->getClient();

        $query = array_merge($query, [
            'shop' => $this->shopId,
            'key'  => $this->shopKey,
        ]);

        $response = $httpClient->post(
            $url,
            [
                'query'       => $query,
                'form_params' => $params,
            ]
        );
        $response = json_decode($response->getBody(), true);

        return $response;
    }
}